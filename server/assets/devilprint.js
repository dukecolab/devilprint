function validateForm() {
    var url = document.forms["printSubmission"]["url"].value;
    var copies = document.forms["printSubmission"]["copies"].value;

    if (url == null || url == "") {
        document.forms["printSubmission"]["command"].value = "file";
    }

    if (copies == null || copies == "" || !(parseInt(copies) >= 1) || !(parseInt(copies) <= 100) || !(/^\d+$/.test(copies))) {
        popUp('Number of copies must be an integer between 1 and 100!','#f08a24');
        return false;
    }

    
}

function popUp(text,color) {
    document.getElementById("alertText").innerHTML = text;
    document.getElementById("popup").style.display = "block";

    document.getElementById("popup").style.backgroundColor = color;
}

document.getElementById("close").onclick = function() {
    document.getElementById("popup").style.display="none";
}

if (location.search.indexOf("success=true") != -1) {
    popUp("Print Job Successfully Requested!",'#f08a24');
}