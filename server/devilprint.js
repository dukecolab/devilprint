var http = require('http');
var https = require('https');
var multer = require('multer');
var unoconv = require('unoconv');
var express = require('express');
var path = require('path');
var crypto = require('crypto');
var fs = require('fs');
var md5 = require('MD5');
var shelljs = require('shelljs');
var wkhtmltopdf = require('wkhtmltopdf');
var reqcount = 0;

//Set up API calls
//api.add("POST","/print",function() {});
/*
Request:
multipart form with file form name being 'fileToPrint'

The rest of the post request is below:
{
	netid: 'none',
	copies: '1',
	duplex: '1',
	pages_per_sheet: '1',
	page_range: '9-12',
	reverse_order: '0',
	url: 'http://www.google.com'
}


*/

// TODO: Implement Real Logging (log4js)
// TODO: Asynchronize/Parallelize what you can within a request.
// TODO: Practices:
	// Verify at function completion that operation was successful
	// Verify/set all inputs at reception of request
	// Verify just before use in actual computation in a function
	// act on error callbacks for shell execution and where they are provided

unoconv.listen(); // Start up unoconv

app = express(); // Create express router

// TODO: Factor out Routes to separate file

app.use(express.static(__dirname + '/assets'));

app.use("/print", multer({ dest: './uploads/', //prep multer for file uploads
  rename: function (fieldname, filename) {
    return md5(filename+(new Date().toISOString())); //build file name from an md5 hash, and add the extension at the end
  }
}));

app.use("/pdf", multer({ dest: './uploads/', //prep multer for file uploads
  rename: function (fieldname, filename) {
    return md5(filename+(new Date().toISOString())); //build file name from an md5 hash, and add the extension at the end
  }
}));

app.get('/',function(req,res) {
	res.sendFile('index2.html', { root: path.join(__dirname, '.') });
});
app.post('/pdf', convertEndpoint);
app.post('/print', printEndpoint);
app.get('/running', statusEndpoint);
app.get('/update', function(req,res) {res.send();});

// TODO: Refactor so all conversions go through 1 conversion function

function printEndpoint(req, res) {
	body = req.body;
	// In case the web interface is used, sends users back to the main page with success=true
	if (body.fromweb == "fromweb" || body.fromweb == "true") {
		res.writeHead(301,{Location: '/?success=true'});
	}
	resolveReqToPdf(req, function printFiles(printPath){
		// TODO: Specific Printer Selection
		printFile(printPath, ['ePrint-OIT', 'ePrint-Color-Library'], body.netid, parseInt(body.copies), parseInt(body.pages_per_sheet), parseInt(body.duplex), body.page_range, function() {
			res.send();
		});
	});

	// TODO: Monitor state of print job in lpstat
}

function convertEndpoint(req, res) {
	body = req.body;
	// In case the web interface is used, sends users back to the main page with success=true
	if (body.fromweb == "fromweb" || body.fromweb == "true") {
		//res.writeHead(301,{Location: '/?success=true'});
	}
	resolveReqToPdf(req, function sendFile(sendPath){
		// TODO: Implement email or direct delivery
		// sendFileToUser(sendpath);
		var filePath = sendPath;
	    var stat = fs.statSync(filePath);

	    res.writeHead(200, {
	        'Content-Type': 'application/pdf',
	        'Content-Length': stat.size
	    });

	    var readStream = fs.createReadStream(filePath);
	    // We replaced all the event handlers with a simple call to readStream.pipe()
	    readStream.pipe(res);
	});
}

function statusEndpoint(req, res) {
	res.writeHead(200,{'Content-Type':'application/json'});
	res.end('{"success": true}');
}

// TODO: Factor out small/basic functions to a separate 'utils.js' file

function webConvertToPdf(url, pdfReadyCallback) {
	// TODO: Any other arguments/headers/footers?
	filename = md5(url+(new Date().toISOString()))+'.pdf';
	// TODO: Verify URL reachability and formatting here
	wkhtmltopdf(url, {output: './uploads/'+filename}, function(code, signal) {
		if (code) {console.log('Wkhtmltopdf Error code: ' + code + ', Signal: ' + signal);}
		pdfReadyCallback(filename);
	});
}

function urlIsHtmlContent(url, ContentTypeFoundCallback) {
	getUrlAttribute(url, 'Content-Type', function(newUrl, contentType) {
		ContentTypeFoundCallback(newUrl, /html/i.test(contentType));
	});
}

function getUrlAttribute(url, attribute, attributeReceivedCallback) {
	// attribute MUST not be dynamically generated! (can't be escaped properly)
	// TODO: Verify url reachability and formatting here
	curlCommand = 'curl -kILs ' + bashEscape(url) + ' | grep -i ^' + attribute + ' | tail -1 | sed -ne \'s/' + attribute + ': \\(.*\\)/\\1/Ip\'';
	var attributeVal = '';
	console.log('Now Executing: ' + curlCommand);
	shelljs.exec(curlCommand, {'silent':true}, function(errCode, stdOut) {
		console.log(stdOut);
		if (errCode || stdOut.length <= 3) {
			console.log(errCode);
			if (url.indexOf('https') != 0) {
				var newUrl = url.replace('http','https')
				getUrlAttribute(newUrl, attribute, attributeReceivedCallback);
				return;
			}
		}
		attributeVal = stdOut;
		attributeReceivedCallback(url, attributeVal);
	});
}

function resolveUrlToFile(url, fileObjReadyCallback) {
	file = {};
	// TODO: Finish this
	file.fieldname = 'fileToPrint';
	file.encoding = '7bit';
	file.truncated = false;
	file.buffer = null;
	urlIsHtmlContent(url, function (newUrl, result) {
		if (result) {
			file.mimetype = 'application/pdf';
			file.extension = "pdf";
			file.originalname = newUrl +'.pdf';
			webConvertToPdf(newUrl, function(filename) {
				file.name = filename;
				// TODO: Fix this using the html content-length header OR pdf size post webConvert
				file.size = 0;
				file.path = "uploads/" + file.name;
				fileObjReadyCallback(file);
			});
		} else { // i.e. if file is a FILE, not a webpage
			file.originalname = getFilenameFromUrl(newUrl);
			file.extension = file.originalname.split('.').pop();
			file.name = md5(file.originalname+(new Date().toISOString())) + '.' + file.extension;
			file.path = "uploads/" + file.name;
			//download file // will need to validate reachability in this
			downloadFile(newUrl, 'uploads/' + file.name, function(err) {
				// TODO: Fix these two lines to use real data
				file.size = 0;
				file.mimetype = '';
				if (err) {console.log(err);}
				fileObjReadyCallback(file);
			});
		}
	});
}

function getFilenameFromUrl(url) {
	//this removes the anchor at the end, if there is one
	url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
	//this removes the query after the file name, if there is one
	url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
	//this removes everything before the last slash in the path
	url = url.substring(url.lastIndexOf("/") + 1, url.length);
	//return
	return url;
}

function resolveFileToPdf(file, pdfReadyCallback) {
	if (!(fs.existsSync(file.path))) {
		console.log('err: File not present:' + file);
		pdfReadyCallback(file.path);
	}

	if (file.extension == 'pdf' || file.mimetype == 'application/pdf') {
		pdfReadyCallback(file.path);
	} else {
		unoconv.convert(file.path, 'pdf', function writeFileToDisk(err, result) {
				if (err) {console.log(err);}
				fs.writeFile(file.path+'.pdf', result, { root: path.join(__dirname, '.') }, function(err) {
					if (err) {console.log(err);}
					pdfReadyCallback(file.path+'.pdf');
				});
		});
	}
}

function resolveReqToPdf(req, pdfReadyCallback) {
	// TODO: Validate inputs (in a separate function)
	body = req.body;

	console.log(body);

	var file = req.files.fileToPrint;

	if (('url' in body) && body.url != '') {
		if (body.url.indexOf('http') != 0) {
			body.url = 'http://' + body.url;
		}
		resolveUrlToFile(body.url, function(file) {
			console.log(file);
			resolveFileToPdf(file, pdfReadyCallback);
		});
	} else {
		console.log(file);
		resolveFileToPdf(file, pdfReadyCallback);
	}
	
}

function printFile(file, printers, netid, copies, pagesPerSheet, duplex, pageRange, donePrintingCallback) {
	// TODO: Add Banner/Header/footer?
	// TODO: Add a flag to delete file after completion
	// TODO: Verify file actually exists (and is a PDF) here
	// TODO: Log output as well
	if (!(fs.existsSync(file.path))) {
		donePrintingCallback(new Error('File not present to be printed: '+ file));
	}
	var copiesString = (copies) ? ' -n ' + bashEscape(copies) : '';
	var ppsString = (pagesPerSheet) ? ' -o number-up=' + bashEscape(pagesPerSheet) : '';
	var duplexString = ((duplex == 0) ? 'one-sided' : 'two-sided-long-edge');
	var sidesString = ' -o sides=' + bashEscape(duplexString);
	var rangeString = (pageRange) ? ' -o page-ranges=' + bashEscape(pageRange) : '';
	for (var i = 0; i < printers.length; i++) {
		var printCommand = 'lp' + ' -E' + ' -d ' + bashEscape(printers[i]) + ' -U ' + bashEscape(netid) + copiesString + ppsString + sidesString + rangeString + ' ' + bashEscape(file);
		console.log('Now Executing: ' + printCommand);
		shelljs.exec(printCommand, {'silent':true}, function(errCode, stdOut) {
			console.log(stdOut);
			if (errCode) {console.log(errCode);}
			if (i >= (printers.length - 1)) {
				donePrintingCallback(null);
			}
		});
	}
}

function downloadFile(url, dest, fileDownloadedCallback) {
	// TODO: verify reachability and formatting of url here
	var file = fs.createWriteStream(dest);
	var protocol = (url.indexOf('https://') == 0 ? https : http);
	var request = protocol.get(url, function(response) {
		response.pipe(file);
		file.on('finish', function() {
			file.close(fileDownloadedCallback);  // close() is async, call cb after close completes.
		});
	}).on('error', function(err) { // Handle errors
		fs.unlink(dest); // Delete the file async. (But we don't check the result)
		if (fileDownloadedCallback) fileDownloadedCallback(err.message);
	});
};

var safePattern = /^[a-z0-9_\/\-.,?:@#%^+=\[\]]*$/i;
var safeishPattern = /^[a-z0-9_\/\-.,?:@#%^+=\[\]{}|&()<>; *']*$/i;
function bashEscape(arg) {
  // These don't need quoting
  if (safePattern.test(arg)) return arg;
 
  // These are fine wrapped in double quotes using weak escaping.
  if (safeishPattern.test(arg)) return '"' + arg + '"';
 
  // Otherwise use strong escaping with single quotes
  return "'" + arg.replace(/'+/g, function (val) {
    // But we need to interpolate single quotes efficiently
 
    // One or two can simply be '\'' -> ' or '\'\'' -> ''
    if (val.length < 3) return "'" + val.replace(/'/g, "\\'") + "'";
 
    // But more in a row, it's better to wrap in double quotes '"'''''"' -> '''''
    return "'\"" + val + "\"'";
 
  }) + "'";
}

app.listen(80); //starts it up
